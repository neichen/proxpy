#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import _thread
from os import urandom

class IDPOOL:
    def __init__(self,length=4):
        self._pool=set()
        self._lock=_thread.allocate_lock()
        self._len=length

    def newid(self):
        with self._lock:
            while (s:=urandom(self._len).hex()) in self._pool:
                pass
            self._pool.add(s)
            return s

    def delid(self,s):
        with self._lock:
            try:
                self._pool.remove(s)
            except:
                pass

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
