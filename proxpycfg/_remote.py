#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import PurePosixPath

import crypto
from dohjson import DNSClient
from http0 import HTTPConnection,isipaddr
try:
    from certfilter import certfilter
except:
    certfilter=None

def setremote(conf,params):
    if params.remote_nohttps:
        ctx=None
    else:
        from ssl import create_default_context
        ctx=create_default_context(cafile=params.cacert)
        ctx.set_alpn_protocols(['http/1.1'])
    rootpath=PurePosixPath('/')/params.remote_path

    with DNSClient(dns_ip=conf['doh']['ip'],
                   dns_port=conf['doh']['port'],
                   dns_name=conf['doh']['name'],
                   dns_path=conf['doh']['path'],
                   cacert=conf['doh']['cacert']) as dns:
        with HTTPConnection(
                params.remote_ip or params.remote_name,port=params.remote_port,
                hostname=params.remote_name,context=ctx,
                tlsname=params.remote_name,dnsclient=dns,certfilter=certfilter,
                sni=not params.remote_nosni) as conn:

            print(f'get public key of {params.remote_name} ...',end='')
            conn.request('GET',(rootpath/'pubkey').as_posix())
            if conn.status!=200:
                print(f' failed ({conn.status})')
                return print(conn.response_io.read(78))
            print(' ok')
            server_pubkey=conn.response_io.read()
            client_pubkey=crypto.generate_pubkey(conf['prikey'])
            token=crypto.encrypt1(client_pubkey,server_pubkey)

            print(f'require signature ...',end='')
            conn.request('POST',(rootpath/'sign').as_posix(),body=token)
            if conn.status!=200:
                print(f' failed ({conn.status})')
                return print(conn.response_io.read(78))
            print(' ok')
            signature=conn.response_io.read()
            return server_pubkey.hex(),signature.hex()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
