#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import _thread
from gzip import decompress
from io import BytesIO,FileIO
from json import dumps,loads
from pathlib import Path,PurePosixPath
from posixpath import normpath
from time import gmtime,strftime,time
from urllib.parse import urlparse

from ._http_connection import HTTPConnection
from ._module_dir import MODULE_DIR

DATA_FMT='%a, %d %b %Y %H:%M:%S GMT'

# cache limit size
CACHE_LIMIT=4*1024*1024

def now():
    return strftime('%FT%TZ',gmtime(time()))

class CacheWrap:

    _accepted_headers=(
        'access-control-allow-origin',
        'content-disposition',
        'content-type',
        'last-modified',
    )

    def __init__(self,*args,expire=None,cachedir=None,cacheroot=MODULE_DIR,
                 backend=None,logger=None,**kwds):
        assert backend is not None
        self._backend=backend(*args,**kwds)
        if callable(logger):
            self._logger=logger
        else:
            self._logger=lambda *a,**k:None
        # seconds to expired, or None for live forever
        self._expire=expire or None
        if cachedir is None:
            self._cachedir=None
        else:
            self._cachedir=cacheroot/cachedir
        self._lock=_thread.allocate_lock()
        self._status=0
        self._response_headers={}
        self._dataio=None

    @property
    def default_headers(self):
        return self._backend.default_headers

    @property
    def headers(self):
        return self._backend.headers

    @property
    def remote_site(self):
        return self._backend.remote_site

    @property
    def closed(self):
        return self._backend.closed

    @property
    def uid(self):
        return self._backend.uid

    @property
    def errmsg(self):
        return self._backend.errmsg

    @property
    def status(self):
        if self._status>199:
            return self._status
        return self._backend.status

    def connect(self):
        pass

    def clear(self):
        self._status=0
        self._response_headers.clear()
        if self._dataio is not None:
            dataio=self._dataio
            self._dataio=None
            dataio.close()

    def close(self):
        self._backend.close()
        self.clear()

    def _get_cache(self,data_path,info_path):
        # return (headers,IOBase) if cached data is responsed, else None
        if not data_path.is_file():return
        mtime=data_path.stat().st_mtime
        if self._expire and time()-mtime>self._expire:return
        headers={}
        try:
            headers.update(loads(info_path.read_text(encoding='utf8')))
        except:
            return
        headers.update([
            ('content-length',data_path.stat().st_size),
            ('date',strftime(DATA_FMT,gmtime(time()))),
        ])
        try:
            iobase=FileIO(data_path,mode='rb')
        except:
            return
        return headers,iobase

    def _accept_header(self,header):
        return header in self._accepted_headers

    @property
    def response_headers(self):
        if self._response_headers:
            return self._response_headers
        return self._backend.response_headers

    def response_iter(self,blocksize=4096):
        if self._dataio is None:
            yield from self._backend.response_iter(blocksize=blocksize)
            return
        while block:=self._dataio.read(blocksize):
            yield block

    def get_backend_api(self,apiname):
        return getattr(self._backend,apiname)

    def request(self,method,path,/,*,body=None,headers={},**kwds):
        try_cache=self._cachedir is not None
        try_cache=try_cache and not path.endswith('/')
        try_cache=try_cache and method=='GET'
        try_cache=try_cache and 'range' not in map(str.lower,headers.keys())
        with self._lock:
            if try_cache:
                cpath=normpath(urlparse(path).path)
                relpath=(PurePosixPath('/')/cpath).relative_to('/')
                data_path=self._cachedir/'data'/self.remote_site/relpath
                info_path=self._cachedir/'info'/self.remote_site/relpath
                if (cache:=self._get_cache(data_path,info_path)) is not None:
                    headers,iobase=cache
                    self._status=200
                    self._response_headers.clear()
                    self._response_headers.update(headers)
                    self._dataio=iobase
                    return self._logger(f'cache wrap read: '
                                        f'{self.remote_site}{path}',self.uid)
                if 'gzip' in headers.get('accept-encoding',''):
                    # only support gzip compress for try cache
                    headers['accept-encoding']='gzip'
            else:
                self._logger(f'cache wrap skip: '
                             f'{self.remote_site}{path}',self.uid)
            self.clear()
            self._backend.request(method,path,body=body,headers=headers,**kwds)
            if not try_cache:
                return
            if self.status!=200:
                # only cache 200 status
                return self._logger(f'cache wrap skip ({self.status}): '
                                    f'{self.remote_site}{path}',self.uid)
            if 'no-store' in self.response_headers.get('cache-control',''):
                # no cache for no-store
                return self._logger(f'cache wrap skip (no-store): '
                                    f'{self.remote_site}{path}',self.uid)
            if self.response_headers.get('transfer-encoding','')=='chunked' and \
               'public' not in self.response_headers.get('cache-control',''):
                # no cache for chunked transfer
                return self._logger(f'cache wrap skip (chunked): '
                                    f'{self.remote_site}{path}',self.uid)
            try:
                size=int(self.response_headers.get('content-length','0'))
            except:
                size=0
            if size>CACHE_LIMIT:
                # no cache for response size over 4M
                size/=1024.0**2
                return self._logger(f'cache wrap skip (size {size:.3f}MiB): '
                                    f'{self.remote_site}{path}',self.uid)
            buflist=[]
            interrupt=False
            try:
                for b in self.response_iter():
                    buflist.append(b)
            except Exception as e:
                self._logger(f'cache wrap failed ({e}): '
                             f'{self.remote_site}{path}',self.uid)
                interrupt=True
            buf=b''.join(buflist)
            if interrupt:
                # incomplete data, no cache
                self._dataio=BytesIO(buf)
                return
            if self.response_headers.get('content-encoding','')=='gzip':
                buf=decompress(buf)
                del self.response_headers['content-encoding']
            self._dataio=BytesIO(buf)
            rheaders={}
            rheaders.update((k,v) for k,v in self.response_headers.items()
                            if self._accept_header(k))
            if 'content-length' in self.response_headers:
                self._response_headers['content-length']=len(buf)
            self._response_headers.update(rheaders)
            data_path.parent.mkdir(parents=True,exist_ok=True)
            data_path.write_bytes(buf)
            info_path.parent.mkdir(parents=True,exist_ok=True)
            info_path.write_text(dumps(rheaders),encoding='utf8')
        return self._logger(f'cache wrap write: '
                            f'{self.remote_site}{path}',self.uid)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
