#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from http0 import CacheWrap

from ._crypto_connection import CryptoConnection
from ._module_dir import MODULE_DIR

class CacheCryptoConnection(CacheWrap):
    def __init__(self,*args,**kwds):
        super().__init__(*args,cacheroot=MODULE_DIR,
                         backend=CryptoConnection,**kwds)

    def setremote(self,*args,**kwds):
        return self.get_backend_api('setremote')(*args,**kwds)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
