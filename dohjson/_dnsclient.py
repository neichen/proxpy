#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import _thread
from json import dumps,loads
from ssl import create_default_context
from urllib.parse import urlunsplit,urlencode

from http0 import HTTPConnection

from ._hosts import resolve_from_hosts
from ._iphelper import IPResult
from ._module_dir import MODULE_DIR
from ._rcode import rcode_str

DEFAULT_IP='1.0.0.1'
DEFAULT_PORT=443
DEFAULT_NAME='cloudflare-dns.com'
DEFAULT_PATH='/dns-query'
# DigiCert Global Root CA
# certificate 1.1.1.1 and 1.0.0.1
DEFAULT_CACERT='DigiCertGlobalRootCA.crt'

class DNSClient(HTTPConnection):

    record_types={4:0x01,6:0x1c}
    query_types={4:'A',6:'AAAA'}

    def __init__(self,dns_ip=None,dns_port=None,dns_name=None,dns_path=None,
                 cacert=None,context=None,timeout=5,blocksize=8192):

        if dns_ip is None:
            dns_ip=DEFAULT_IP
            dns_name=DEFAULT_NAME
        if dns_port is None:
            dns_port=DEFAULT_PORT
        self.path=DEFAULT_PATH if dns_path is None else dns_path
        if context is None:
            if cacert is None:
                cacert=DEFAULT_CACERT
            cacert=MODULE_DIR/cacert
            context=create_default_context(cadata=cacert.read_text())

        super().__init__(dns_ip,port=dns_port,hostname=dns_name,
                         timeout=timeout,blocksize=blocksize,
                         tlsname=dns_name,context=context)
        self.default_headers={
            'accept-encoding':'gzip',
            'accept':'application/dns-json',
        }
        self.default_query=[('do','true'),('cd','false')]
        self._cache={4:{},6:{}}

        self._lock=_thread.allocate_lock()

    def _lookup(self,name,code):
        if (ips:=resolve_from_hosts(name,code)):
            return ips
        if name in self._cache[code]:
            if (ips:=self._cache[code][name].getip()):
                return ips
            del self._cache[code][name]
        query=[('name',name),('type',self.query_types[code]),*self.default_query]
        self.request('GET',
                     urlunsplit(('','',self.path,urlencode(query),'')),
                     headers=self.default_headers)
        # remote server error
        if self.status>399:
            self.close()
            if self.status>599:
                raise Exception(f'connection failed: {self.response_io.read()}')
            if self.status>499:
                raise Exception(f'server down: {self.status}')
            raise Exception(f'server refused: {self.status}')
        contype=self.response_headers.get('content-type','')
        if contype!='application/dns-json':
            raise Exception(f'server response unknown type: {contype}')
        result=loads(self.response_io.read())
        # name error
        if (rcode:=result['Status']):
            name,reason=rcode_str(rcode)
            raise Exception(f'{name}({rcode}): {reason}')
        # no answer
        if 'Answer' not in result:
            raise Exception(f'no answer')
        self._cache[code][name]=IPResult(name)
        ips=[]
        for a in result['Answer']:
            # ignore not asked type
            if a['type']!=self.record_types[code]:continue
            self._cache[code][name].add(a['data'],a['TTL'])
            ips.append(a['data'])
        return ips

    def lookup(self,name,disable_ipv6=False,prefer_ipv6=False,force_ipv6=False):
        if not name.endswith('.'):name+='.'
        required_types=[]
        if not force_ipv6:
            required_types.append(4)
        if not disable_ipv6:
            required_types.append(6)
        if prefer_ipv6:
            required_types.reverse()
        with self._lock:
            ipaddrs={4:[],6:[]}
            for code in required_types:
                ips=self._lookup(name,code)
                if ips is None:
                    self.close()
                    continue
                ipaddrs[code][:]=ips
        try:
            return tuple(tuple(ipaddrs[c]) for c in required_types)
        finally:
            ipaddrs.clear()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
