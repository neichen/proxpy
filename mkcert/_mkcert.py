#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from ipaddress import ip_address
from os import urandom
from pathlib import Path
from tempfile import TemporaryDirectory

from ._cnf import openssl_cnf
from ._module_dir import MODULE_DIR
from ._openssl import openssl

CERTPATH='certs'

_LOGLEVEL={
    'debug':False,
    'info':False,
    'warning':True,
}

keysfx='.key'
csrsfx='.csr'
crtsfx='.crt'

def debug(s):
    if _LOGLEVEL['debug']:
        print(s)

def info(s):
    if _LOGLEVEL['info']:
        print(s)

def warning(s):
    if _LOGLEVEL['warning']:
        print(s)

def err(s):
    raise ValueError(s)

def writecfg(path,**kwds):
    params=dict(
        root=None,
        md=None,
        commonname=None,
        dnsname=None,
        days=None,
    )
    params.update(kwds)
    cnf=openssl_cnf.format(**params)
    debug(path)
    debug(cnf)
    return path.write_text(cnf,encoding='utf8')

def isipaddr(s):
    try:
        ipaddr=ip_address(s)
    except ValueError:
        return False
    else:
        if ipaddr.version in (4,6):
            return True
        return False

def genkey(name,/,*,keyalg=None,keyopt={},
           overwrite=False,cwd=None):
    if not name:return err('key name is empty')
    keyname=name+keysfx
    keyfile=Path(keyname) if cwd is None else Path(cwd)/keyname
    if keyfile.is_file() and not overwrite:
        debug(f'use existing private key {keyname}')
        return keyfile
    options=['-algorithm',keyalg,'-out',keyfile]
    if keyopt:
        for k,v in keyopt.items():
            options.extend(('-pkeyopt',f'{k}:{v}'))
    openssl('genpkey',*options)
    info(f'create new private key {keyname}')
    return keyfile

def gencacert(name,keyfile,confdir,/,*,expire=0,md=None,
              overwrite=False,cwd=None):
    if not name:return err('ca name is empty')
    caname=name+crtsfx
    cafile=Path(caname) if cwd is None else Path(cwd)/caname
    if cafile.is_file() and not overwrite:
        debug(f'use existing cacert {caname}')
        return cafile
    conffile=confdir/'cacert.cnf'
    writecfg(conffile,
             root=confdir.as_posix(),
             md=md,
             commonname=f'{name}')
    openssl('req','-new','-x509','-days',str(expire),
            '-config',conffile,'-extensions','ca_exts',
            '-key',keyfile,'-out',cafile)
    info(f'create new cacert {caname}')
    return cafile

def genservcsr(name,keyfile,confdir,/,*,expire=7,md=None,
               overwrite=False):
    if not name:return err('server name is empty')
    csrname=name+csrsfx
    csrfile=confdir/csrname
    if csrfile.is_file() and not overwrite:
        debug(f'use existing cert request {csrfile}')
        return csrfile
    conffile=confdir/'servcsr.cnf'
    writecfg(conffile,
             root=confdir.as_posix(),
             days=expire,
             md=md,
             key=keyfile.as_posix(),
             commonname=f'{name}')
    openssl('req','-new',
            '-config',conffile,'-reqexts','req_exts',
            '-key',keyfile,'-out',csrfile)
    info(f'create new generate cert request {csrfile}')
    return csrfile

def genservcrt(name,csrfile,cafile,cakeyfile,confdir,/,*,
               dnslist=[],expire=0,md=None,
               overwrite=False,cwd=None):
    if not name:return err('server name is empty')
    crtname=name+crtsfx
    crtfile=Path(crtname) if cwd is None else Path(cwd)/crtname
    if crtfile.is_file() and not overwrite:
        debug(f'use existing server cert {crtfile}')
        return crtfile
    (confdir/'index.txt').write_bytes(b'')
    (confdir/'index.txt.attr').write_bytes(b'')
    (confdir/'serial').write_text(urandom(16).hex(),encoding='utf8')
    conffile=confdir/'servcrt.cnf'
    writecfg(conffile,
             root=confdir.as_posix(),
             days=expire,
             md=md,
             servername=f'{name}',
             dnsname=','.join((f'IP:{c}' if isipaddr(c) else f'DNS:{c}')
                              for c in dnslist))
    openssl('ca','-batch','-config',conffile,'-extensions','srv_exts',
            '-cert',cafile,'-keyfile',cakeyfile,
            '-in',csrfile,'-out',crtfile)
    info(f'create new generate server cert {crtfile}')
    return crtfile

def mkcert(params):
    target=(MODULE_DIR/CERTPATH) if params.servdir is None else params.servdir
    target.mkdir(parents=True,exist_ok=True)
    if params.cadir is not None:
        params.cadir.mkdir(parents=True,exist_ok=True)
    with TemporaryDirectory(dir=target,suffix='-mkcert') as _tmpdir:
        tmpdir=Path(_tmpdir)
        (tmpdir/'.rnd').write_bytes(b'')

        keyopt={}
        if params.keyalg=='ec':
            keyopt['ec_paramgen_curve']=params.curve

        cakey=genkey(params.ca,
                     keyalg=params.keyalg,keyopt=keyopt,
                     overwrite=params.overwrite_ca,cwd=params.cadir)
        cacert=gencacert(params.ca,cakey,tmpdir,
                         expire=params.cadays,md=params.md,
                         overwrite=params.overwrite_ca,cwd=params.cadir)

        if params.serv:
            servkey=genkey(params.serv,
                           keyalg=params.keyalg,keyopt=keyopt,
                           overwrite=params.overwrite_serv,cwd=target)
            servcsr=genservcsr(params.serv,servkey,tmpdir,
                               expire=params.servdays,md=params.md,
                               overwrite=params.overwrite_serv)
            servcrt=genservcrt(params.serv,servcsr,cacert,cakey,tmpdir,
                               expire=params.servdays,md=params.md,
                               dnslist=params.dnslist,cwd=target)
            return servcrt,servkey

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
